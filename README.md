# rls

#### 项目介绍
基于学习目的，使用Rust语言编写的ls命令

#### 软件架构
软件架构说明


#### 安装教程

1. 克隆代码
``` shell
git clone https://gitee.com/dev-tang/rls.git
```
2. 编译
``` shell
cd rls && cargo build
```

#### 使用说明

``` shell
rls.exe
```
``` shell
2018-08-16 10:24:37                 0  .cargo-lock
2018-08-16 10:24:37   <DIR>            .fingerprint
2018-08-16 10:24:37   <DIR>            build
2018-08-16 10:24:46   <DIR>            deps
2018-08-16 10:24:37   <DIR>            examples
2018-08-16 10:24:45   <DIR>            incremental
2018-08-16 10:24:37   <DIR>            native
2018-08-16 10:32:34                98  rls.d
2018-08-16 10:24:46            981504  rls.exe
2018-08-16 10:24:46           4000768  rls.pdb
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
